<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Demographic::class, function (Faker $faker) {
    return [
        'gender' => 1,
        'birth_date' => now(),
        'completed_diagnosis_date' => now(),
        'province' => 1,
        'district' => 1,
        'gp_mp' => 2,
        'gp_mp_name' => $faker->name,
        'ward' => '12',
        'village' => $faker->address,
        'stay_no' => '100',
        'marital_status' => 1,
        'ethnic_group' => 1,
        'religion' => 1,
        'education' => 1,
        'occupation' => 'Farmer',
    ];
});
