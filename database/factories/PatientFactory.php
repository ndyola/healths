<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Patient::class, function (Faker $faker) {
    return [
        'registration_no' => str_random(10),
        'certificate_type' => 1,
        'certificate_no' => str_random(10),
        'certificate_issue_date' => now(),
        'certificate_issue_place' => $faker->address,
        'national_identity_no' => str_random(10),
        'patient_name' => $faker->name,
        'relative_name' => $faker->name,
        'gender' => $faker->randomElement(['male', 'female']),
        'relation_with_patient' => 'Friend',
        'phone_no' => str_random(10),
        'alternative_phone_no' => str_random(10),
        'collected_date' => now(),
        'collected_by' =>$faker->name,
        'approved_by' => $faker->name,
        'remarks' => $faker->name
    ];
});
