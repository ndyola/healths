<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Source::class, function (Faker $faker) {
    return [
        'source_date' => now(),
        'type' => 'Hospice',
        'name' => $faker->name,
        'file_no' => str_random(10),
        'demographic_information' => 'Yes',
        'diagnosis_information' => 'Yes',
        'clinical_information' => 'Yes',
        'follow_up_information' => 'Yes'
    ];
});
