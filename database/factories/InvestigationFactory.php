<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Investigation::class, function (Faker $faker) {
    return [
        'clinical_diagnosis' => $faker->company,
        'date_of_diagnosis' => now(),
        'basis_of_diagnosis' => 1,
        'primary_site' => $faker->company,
        'primary_site_code' => str_random(10),
        'literality' => 1,
        'morphological_diagnosis' => $faker->name,
        'morphological_diagnosis_code' => str_random(10),
        'behavior' => 1,
        'grade' => 1,
        'extent_of_disease' => 1,
        'tnm_tumor' => str_random(10),
        'tnm_node' => str_random(10),
        'tnm_metasis' => str_random(10),
        'stages' => 1,
        'treatment' => '1,2,3',
        'treatment_completion' => 1,
        'secondary_site' => 1,
        'date_of_death' => now(),
        'cause_of_death' => 'Cancer',
        'place_of_death' => $faker->address,
        'multiple_primary_tumors' => 1
    ];
});
