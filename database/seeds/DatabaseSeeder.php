<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);

        factory(App\Patient::class, 50)->create()->each(function($patient) {
            // $patient->demographic()->save(factory(App\Demographic::class)->make());
            // $patient->investigation()->save(factory(App\Investigation::class)->make());
            // $patient->followup()->save(factory(App\Followup::class)->make());
            // $patient->source()->save(factory(App\Source::class)->make());
          });

    }
}
