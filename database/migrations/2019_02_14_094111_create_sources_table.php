<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sources', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patient_id');
            $table->date('source_date')->nullable();
            $table->string('type')->nullable();
            $table->string('name')->nullable();
            $table->string('file_no')->nullable();
            $table->string('demographic_information')->nullable();
            $table->string('diagnosis_information')->nullable();
            $table->string('clinical_information')->nullable();
            $table->string('cross_verify')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sources');
    }
}
