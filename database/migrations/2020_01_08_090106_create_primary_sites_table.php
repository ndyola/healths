<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrimarySitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('primary_sites', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patient_id')->unsigned();
            $table->string('clinical_diagnosis')->nullable();
            $table->date('date_of_first_diagnosis')->nullable();
            $table->string('basis_of_diagnosis')->nullable();
            $table->string('primary_site')->nullable();
            $table->string('primary_site_code')->nullable();
            $table->string('microscopic_finding_primary_site')->nullable();
            $table->string('microscopic_finding_primary_site_code')->nullable();

            $table->string('laterality')->nullable();
            $table->string('morphological_diagnosis')->nullable();
            $table->string('morphological_diagnosis_code')->nullable();

            $table->string('behavior')->nullable();
            $table->string('grade')->nullable();
            //staging
            //tmt_classification
            $table->string('tnm_tumor')->nullable();
            $table->string('tnm_node')->nullable();
            $table->string('tnm_metasis')->nullable();
            $table->string('pr_staging_figo')->nullable();
            $table->string('pr_staging_Arbor')->nullable();
            $table->string('pr_staging_composite')->nullable();
            $table->string('pr_staging_other')->nullable();
            $table->string('extent_of_disease')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('primary_sites');
    }
}
