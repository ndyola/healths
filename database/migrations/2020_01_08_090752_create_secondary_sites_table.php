<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecondarySitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('secondary_sites', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patient_id')->unsigned();
            $table->date('date_of_diagnosis')->nullable();
            $table->string('secondary_metastasis_site')->nullable();
            $table->string('secondary_metastasis_site_specify')->nullable();
            $table->string('microscopic_finding_secondary_site')->nullable();
            $table->string('microscopic_finding_secondary_site_code')->nullable();
            $table->string('secondary_behaviour')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('secondary_sites');
    }
}
