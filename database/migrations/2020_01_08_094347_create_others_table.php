<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOthersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('others', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patient_id')->unsigned();
            $table->date('death_date')->nullable();
            $table->string('cause_of_death')->nullable();
            $table->string('death_specify')->nullable();
            $table->string('place_of_death')->nullable();
            
            $table->string('remarks')->nullable();
            $table->string('collected_by')->nullable();
            $table->string('collected_date')->nullable();
            $table->string('approved_by')->nullable();
            $table->string('approved_date')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('others');
    }
}
