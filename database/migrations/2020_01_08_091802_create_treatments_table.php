<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTreatmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treatments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patient_id')->unsigned();
            $table->date('treatment_date')->nullable();
            
            $table->string('tmt_tumor')->nullable();
            $table->string('tmt_node')->nullable();
            $table->string('tmt_metasis')->nullable();
            $table->string('stages_figo_gyn')->nullable();
            $table->string('treatment_given')->nullable();
            $table->string('treatment_type')->nullable();
            $table->string('treatment')->nullable();
            $table->string('treatmentStatus')->nullable();
            $table->string('treatment_completion')->nullable();
            $table->string('status_of_last_contact')->nullable();
            $table->date('date_of_last_contact')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('treatments');
    }
}
