<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestigationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investigations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patient_id');
            $table->string('clinical_diagnosis')->nullable();
            $table->date('date_of_diagnosis')->nullable();
            $table->string('basis_of_diagnosis')->nullable();
            $table->string('primary_site')->nullable();
            $table->string('primary_site_code')->nullable();
            $table->string('literality')->nullable();
            $table->string('morphological_diagnosis')->nullable();
            $table->string('morphological_diagnosis_code')->nullable();
            $table->string('behavior')->nullable();
            $table->string('grade')->nullable();
            $table->string('extent_of_disease')->nullable();
            $table->string('tnm_tumor')->nullable();
            $table->string('tnm_node')->nullable();
            $table->string('tnm_metasis')->nullable();
            $table->string('stages')->nullable();
            $table->string('treatment')->nullable();
            $table->string('treatment_completion')->nullable();
            $table->string('secondary_site')->nullable();
            $table->date('date_of_death')->nullable();
            $table->string('cause_of_death')->nullable();
            $table->string('place_of_death')->nullable();
            $table->string('multiple_primary_tumors')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investigations');
    }
}
