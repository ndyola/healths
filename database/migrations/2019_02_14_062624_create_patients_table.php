<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('registration_no')->nullable();
            $table->string('file_no')->nullable();
            $table->string('national_identity_no')->nullable();
            
            $table->string('patient_first_name')->nullable();
            $table->string('patient_middle_name')->nullable();
            $table->string('patient_last_name')->nullable();

            $table->date('birth_date')->nullable();
            $table->integer('age')->nullable();
            $table->string('gender')->nullable();

            $table->string('marital_status')->nullable();
            $table->string('ethnic_group')->nullable();
            $table->string('religion')->nullable();
            $table->string('education')->nullable();
            $table->string('occupation')->nullable();
            $table->string('nationality')->nullable();

            $table->string('certificate_type')->nullable();
            $table->string('certificate_no')->nullable();
            $table->date('certificate_issue_date')->nullable();;
            $table->string('certificate_issue_place')->nullable();
           
            $table->string('cr_province')->nullable();
            $table->string('cr_district')->nullable();
            // $table->string('gp_mp')->nullable();
            $table->string('cr_gp_mp_name')->nullable();
            $table->string('cr_ward')->nullable();
            $table->string('cr_village')->nullable();

            $table->string('pr_province')->nullable();
            $table->string('pr_district')->nullable();
            // $table->string('gp_mp')->nullable();
            $table->string('pr_gp_mp_name')->nullable();
            $table->string('pr_ward')->nullable();
            $table->string('pr_village')->nullable();
             $table->string('phone_no')->nullable();
            $table->string('alternative_phone_no')->nullable();


            $table->string('relative_name')->nullable();
            $table->string('relation_with_patient')->nullable();
            $table->string('year_at_age_of_diagnosis')->nullable();
            $table->string('stay_no')->nullable();

            // $table->string('collected_date')->nullable();
            // $table->string('collected_by')->nullable();
            // $table->string('approved_by')->nullable();
            // $table->longText('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
