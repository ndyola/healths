<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->middleware('auth')->name('home');
Route::get('/users', 'UserController@index')->middleware('auth')->name('home');
Route::get('/admin', 'HomeController@index')->middleware('auth')->name('home');

Route::get('/admin/{catch_all}', 'HomeController@index')->where('catchall', '(.*)')->middleware('auth');
Route::get('/admin/{catch_all}/{all}', 'HomeController@index')->where(['catchall' => '(.*)', 'all' => '(.*)'])->middleware('auth');
// Route::post('/form','Auth\PatientController@store')->middleware('auth');
Route::post('/patientidentify', 'PatientController@store')->middleware('auth');
Route::put('/patientidentify/{id}', 'PatientController@update')->middleware('auth');
Route::get('/primarysite', 'PrimarySiteController@index')->middleware('auth');
Route::get('/primarysite/{id}', 'PrimarySiteController@show')->middleware('auth');
Route::post('/primarysite', 'PrimarySiteController@store')->middleware('auth');
Route::delete('/primarysite/{id}', 'PrimarySiteController@destroy')->middleware('auth');
Route::put('/primarysite/{id}', 'PrimarySiteController@update')->middleware('auth');
Route::get('/secondarysite', 'SecondarySiteController@index')->middleware('auth');
Route::get('/secondarysite/{id}', 'SecondarySiteController@show')->middleware('auth');
Route::post('/secondarysite', 'SecondarySiteController@store')->middleware('auth');
Route::delete('/secondarysite/{id}', 'SecondarysiteController@destroy')->middleware('auth');
Route::put('/secondarysite/{id}', 'SecondarysiteController@update')->middleware('auth');
Route::get('/treatment', 'TreatmentController@index')->middleware('auth');
Route::get('/treatment/{id}', 'TreatmentController@show')->middleware('auth');

Route::post('/treatment', 'TreatmentController@store')->middleware('auth');
Route::delete('/treatment/{id}', 'TreatmentController@destroy')->middleware('auth');
Route::put('/treatment/{id}', 'TreatmentController@update')->middleware('auth');
Route::post('/follow', 'FollowupController@store')->middleware('auth');
Route::get('/follow', 'FollowupController@index')->middleware('auth');
Route::get('/follow/{id}', 'FollowupController@show')->middleware('auth');

Route::delete('/follow/{id}', 'FollowupController@destroy')->middleware('auth');
Route::put('/follow/{id}', 'FollowupController@update')->middleware('auth');
Route::get('/source', 'SourceController@index')->middleware('auth');
Route::get('/source/{id}', 'SourceController@show')->middleware('auth');
Route::post('/source', 'SourceController@store')->middleware('auth');
Route::delete('/source/{id}', 'SourceController@destroy')->middleware('auth');
Route::put('/source/{id}', 'SourceController@update')->middleware('auth');
Route::post('/others', 'OtherController@store')->middleware('auth');

Route::get('/login', '\App\Http\Controllers\Auth\LoginController@showLoginForm')->name('login');

Route::post('/login', '\App\Http\Controllers\Auth\LoginController@login');

Route::post('/logout','\App\Http\Controllers\Auth\LoginController@logout')->middleware('auth')->name('logout');

Route::get('logout','Auth\LoginController@logout')->middleware('auth');

Route::get('/register','\App\Http\Controllers\Auth\RegisterController@showRegistrationForm')->middleware('auth');

Route::post('/register','\App\Http\Controllers\Auth\RegisterController@register')->middleware('auth');

Route::post('/user/change-password','Auth\ResetPasswordController@resetUserPassword')->middleware('auth');

Route::get('/export-pdf/{id}','PatientController@export_pdf')->middleware('auth');

Route::resource('/patient', 'PatientController')->except('edit')->middleware('auth');
Route::post('patient/delete-requests', 'PatientController@bulkDelete')->middleware('auth');
Route::get('patient/search-patients/{search}', 'PatientController@searchPatient')->middleware('auth');