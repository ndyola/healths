<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Http\Requests\CreatePatientRequest;
use App\{User, Patient ,Secondary_site};


class SecondarySiteController extends Controller
{
    public function store(Request $request)
    {
        $secondary_site = Secondary_site::create([
            'patient_id' =>isset($request->data['patient_id'])? $request->data['patient_id'] : null,
            'date_of_diagnosis'=>isset($request->data['date_of_diagnosis'])? $request->data['date_of_diagnosis'] : null,
            'secondary_metastasis_site'=>isset($request->data['secondary_metastasis_site'])? $request->data['secondary_metastasis_site'] : null,
            'secondary_metastasis_site_specify'=>isset($request->data['secondary_metastasis_site_specify'])? $request->data['secondary_metastasis_site_specify'] : null,
            'microscopic_finding_secondary_site'=>isset($request->data['microscopic_finding_secondary_site'])? $request->data['microscopic_finding_secondary_site'] : null,
            'microscopic_finding_secondary_site_code'=>isset($request->data['microscopic_finding_secondary_site_code'])? $request->data['microscopic_finding_secondary_site_code'] : null,
            'secondary_behaviour'=>isset($request->data['secondary_behaviour'])? $request->data['secondary_behaviour'] : null,
        ]);
       
        
         return response()->json('success',200);
    }
    public function index()
    {
        
       $secondary_site = Secondary_site::orderby('patient_id', 'desc')->paginate(20);
        return response()->json($secondary_site);
  
    }
    public function update(Request $request, $id)
    {
        $secondary_site = Secondary_site::find($id);
        $secondary_site->share_name = $request->get('share_name');
        
        $secondary_site->save();

    }
    public function destroy($id)
    {
       
        $secondary_site = Secondary_site::find($id);
         $secondary_site->delete();
        return response()->json('success',200);
    }
    public function show($id)
    {
        $secondary_site = Secondary_site::where('patient_id',$id)->get();
        return response()->json($secondary_site);

    }
}
