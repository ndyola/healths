<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $app = [
            "name" => config('app.name')
        ];
        return view('admin.layouts.default')->with([
            'user' => json_encode(auth()->user()),
            'app' => json_encode($app)
        ]);
    }
}
