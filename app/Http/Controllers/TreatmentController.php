<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Http\Requests\CreatePatientRequest;
use App\{User, Patient ,Treatment};


class TreatmentController extends Controller
{
    public function store(Request $request)
    {
        
        $treatment = Treatment::create([
            'patient_id' =>isset($request->data['patient_id'])? $request->data['patient_id'] : null,
            'tmt_tumor'=>isset($request->data['tmt_tumor'])? $request->data['tmt_tumor'] : null,
            'tmt_node'=>isset($request->data['tmt_node'])? $request->data['tmt_node'] : null,
            'tmt_metasis'=>isset($request->data['tmt_metasis'])? $request->data['tmt_metasis'] : null,
            'treatment_given'=>isset($request->data['treatment_given'])? $request->data['treatment_given'] : null,
            'treatment_type'=>isset($request->data['treatment_type'])? $request->data['treatment_type'] : null,
            'treatment'=>isset($request->data['treatment'])? $request->data['treatment'] : null,
            'treatmentStatus'=>isset($request->data['treatmentStatus'])? $request->data['treatmentStatus'] : null,
            'treatment_completion'=>isset($request->data['treatment_completion'])? $request->data['treatment_completion'] : null,
            'status_of_last_contact'=>isset($request->data['status_of_last_contact'])? $request->data['status_of_last_contact'] : null,
            'date_of_last_contact'=>isset($request->data['date_of_last_contact'])? $request->data['date_of_last_contact'] : null,
            'treatment_date'=>isset($request->data['treatment_date'])? $request->data['treatment_date'] : null,
'stages_figo_gyn'=>isset($request->data['stages_figo_gyn'])? $request->data['stages_figo_gyn'] : null,
        ]);
        
         return response()->json('success',200);
    }
    public function index()
    {
        
       $treatment = Treatment::orderby('patient_id', 'desc')->paginate(20);
        return response()->json($treatment);
  
    }
    public function update(Request $request, $id)
    {
        $treatment = Treatment::find($id);
        $treatment->share_name = $request->get('share_name');
        
        $treatment->save();

    }
    public function destroy($id)
    {
       
        $treatment = Treatment::find($id);
         $treatment->delete();
        return response()->json('success',200);
    }
    public function show($id)
    {
        $treatment = Treatment::where('patient_id',$id)->get();
        return response()->json($treatment);

    }
}
