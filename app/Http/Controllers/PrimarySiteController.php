<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \App\Http\Requests\CreatePatientRequest;
use App\{User, Patient , Primary_site};

class PrimarySiteController extends Controller
{
    public function store(Request $request)
    {
        
        $primary_site = Primary_site::create([
            
            'patient_id' =>isset($request->data['patient_id'])? $request->data['patient_id'] : null,
            'clinical_diagnosis' => isset($request->data['clinical_diagnosis'])? $request->data['clinical_diagnosis'] : null,
            'date_of_first_diagnosis' => isset($request->data['date_of_diagnosis'])? $request->data['date_of_diagnosis'] : null,
            'basis_of_diagnosis' => isset($request->data['basis_of_diagnosis'])? $request->data['basis_of_diagnosis'] : null,
            'primary_site' => isset($request->data['primary_site'])? $request->data['primary_site'] : null,
            'primary_site_code' => isset($request->data['primary_site_code'])? $request->data['primary_site_code'] : null,
            'laterality' => isset($request->data['laterality'])? $request->data['laterality'] : null,
            'microscopic_finding_primary_site' => isset($request->data['microscopic_finding_primary_site'])? $request->data['microscopic_finding_primary_site'] : null,
            'microscopic_finding_primary_site_code' => isset($request->data['microscopic_finding_primary_site_code'])? $request->data['microscopic_finding_primary_site_code'] : null,
            'morphological_diagnosis' => isset($request->data['morphological_diagnosis'])? $request->data['morphological_diagnosis'] : null,
            'morphological_diagnosis_code' => isset($request->data['morphological_code'])? $request->data['morphological_code'] : null,
            'behavior' => isset($request->data['behavior'])? $request->data['behavior'] : null,
            'grade' => isset($request->data['grade'])? $request->data['grade'] : null,
            'tnm_tumor' => isset($request->data['tnm_tumor'])? $request->data['tnm_tumor'] : null,
            'tnm_node' => isset($request->data['tnm_node'])? $request->data['tnm_node'] : null,
            'tnm_metasis' => isset($request->data['tnm_metasis'])? $request->data['tnm_metasis'] : null,
            'extent_of_disease' => isset($request->data['extent_of_disease'])? $request->data['extent_of_disease'] : null,
            'pr_staging_figo' => isset($request->data['pr_staging_figo'])? $request->data['pr_staging_figo'] : null,
            'pr_staging_Arbor' => isset($request->data['pr_staging_Arbor'])? $request->data['pr_staging_Arbor'] : null,
            'pr_staging_composite' => isset($request->data['pr_staging_composite'])? $request->data['pr_staging_composite'] : null,
            'pr_staging_other' => isset($request->data['pr_staging_other'])? $request->data['pr_staging_other'] : null
    

        ]);
  
         return response()->json('success',200);
    }
    public function index()
    {
        
       $primary_site = Primary_site::orderby('patient_id', 'desc')->paginate(20);
        return response()->json($primary_site);
  
    }
    public function update(Request $request, $id)
    {
        $primary_site = Primary_site::find($id);
        $primary_site->share_name = $request->get('share_name');
        $primary_site->share_price = $request->get('share_price');
        $primary_site->share_qty = $request->get('share_qty');
        $primary_site->patient_id=$request->data['patient_id'];
        $primary_site->clinical_diagnosis=$request->data['clinical_diagnosis'];
        $primary_site->date_of_first_diagnosis=$request->data['date_of_diagnosis'];
        $primary_site->basis_of_diagnosis=$request->data['basis_of_diagnosis'];
        $primary_site->primary_site=$request->data['primary_site'];
        $primary_site->primary_site_code=$request->data['primary_site_code'];
        $primary_site->laterality=$request->data['laterality'];
        $primary_site->microscopic_finding_primary_site=$request->data['microscopic_finding_primary_site'];
        $primary_site->microscopic_finding_primary_site_code=$request->data['microscopic_finding_primary_site_code'];
        $primary_site->morphological_diagnosis=$request->data['morphological_diagnosis'];
        $primary_site->morphological_diagnosis_code=$request->data['morphological_code'];
        $primary_site->behavior=$request->data['behavior'];
        $primary_site->grade=$request->data['grade'];
        $primary_site->tnm_tumor=$request->data['tnm_tumor'];
        $primary_site->tnm_node=$request->data['tnm_node'];
        $primary_site->tnm_metasis=$request->data['tnm_metasis'];
        $primary_site->extent_of_disease=$request->data['extent_of_disease'];
        $primary_site->pr_staging_figo=$request->data['pr_staging_figo'];
        $primary_site->pr_staging_Arbor=$request->data['pr_staging_Arbor'];
        $primary_site->pr_staging_composite=$request->data['pr_staging_composite'];
        $primary_site->pr_staging_other=$request->data['pr_staging_other'];
        $primary_site->save();

    }
    public function show($id)
    {
        $primary_site = Primary_site::where('patient_id',$id)->get();
        return response()->json($primary_site);

    }
    public function destroy($id)
    {
       
        $primary_site = Primary_site::find($id);
         $primary_site->delete();
        return response()->json('success',200);
    }
}
