<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Http\Requests\CreatePatientRequest;
use App\{User, Patient , Source};

class SourceController extends Controller
{
    public function store(Request $request)
    {
        $source = Source::create([

            'patient_id' =>isset($request->data['patient_id'])? $request->data['patient_id'] : null,
            'source_date' =>isset($request->data['source_date'])? $request->data['source_date'] : null,
            'type'=>isset($request->data['type'])? $request->data['type'] : null,
            'name'=>isset($request->data['name'])? $request->data['name'] : null,
            'file_no'=>isset($request->data['file_no'])? $request->data['file_no'] : null,
            'demographic_information'=>isset($request->data['demographic_information'])? $request->data['demographic_information'] : null,
            'clinical_information'=>isset($request->data['clinical_information'])? $request->data['clinical_information'] : null,
            'cross_verify'=>isset($request->data['cross_verify'])? $request->data['cross_verify'] : null,
            
        ]);
       
        
         return response()->json('success',200);
    }
    public function index()
    {
        
       $source = Source::orderby('patient_id', 'desc')->paginate(20);
        return response()->json($source);
  
    }
    public function update(Request $request, $id)
    {
        $source = Source::find($id);
        $source->share_name = $request->get('share_name');
        
        $source->save();

    }
    public function destroy($id)
    {
       
        $source = Source::find($id);
         $source->delete();
        return response()->json('success',200);
    }
    public function show($id)
    {
        $source = Source::where('patient_id',$id)->get();
        return response()->json($source);

    }
    
}
