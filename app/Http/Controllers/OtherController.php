<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Http\Requests\CreatePatientRequest;
use App\{User, Patient ,Other};

class OtherController extends Controller
{
    public function store(Request $request)
    {
        
       $other = Other::create([
            'patient_id' =>isset($request->data['patient_id'])? $request->data['patient_id'] : null,
            'death_date' => isset($request->data['date_of_death'])? $request->data['date_of_death'] : null,
            'cause_of_death' => isset($request->data['cause_of_death'])? $request->data['cause_of_death'] : null,
            'death_specify' => isset($request->data['death_specify'])? $request->data['death_specify'] : null,
            'place_of_death' => isset($request->data['place_of_death'])? $request->data['place_of_death'] : null,
            'collected_by' => isset($request->data['collected_by'])? $request->data['collected_by'] : null,
              'collected_date' => isset($request->data['collected_date'])? $request->data['collected_date'] : null,
            'approved_by' => isset($request->data['approved_by'])? $request->data['approved_by'] : null,
              'approved_date' => isset($request->data['approved_date'])? $request->data['approved_date'] : null,
            'remarks' => isset($request->data['remarks'])? $request->data['remarks'] : null,
            ]);
         return response()->json('success',200);
    }
}
