<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('resetUserPassword');
    }

    public function resetUserPassword(Request $request)
    {
        if($this->middleware('auth')){
            if (password_verify($request->password ,auth()->user()->password)) {
                auth()->user()->update([
                    'password' => Hash::make($request->new_password) 
                ]);
                return response()->json(["message" => "Password Has Been Changed"],200);
            }else{
                return response()->json(["message" => "Sorry password cannot be matched"],422);
            }
        }
    }
}
