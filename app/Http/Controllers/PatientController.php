<?php

namespace App\Http\Controllers;

use PDF;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use \App\Http\Requests\CreatePatientRequest;
use App\{User, Patient , Primary_site,Secondary_site,Treatment,Source, Followup,Other, Demographic, Investigation};

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        
        $patients = Patient::orderby('updated_at', 'desc')->get();
        return response()->json($patients);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePatientRequest $request)
    {

        $patient = Patient::create([
            'registration_no' => isset($request->data['registration_no'])? $request->data['registration_no'] : null,
            'file_no' => isset($request->data['national_file_no'])? $request->data['national_file_no'] : null,
            'national_identity_no' => isset($request->data['national_identity_no'])? $request->data['national_identity_no'] : null,
            'patient_first_name' => isset($request->data['patient_first_name'])? $request->data['patient_first_name'] : null,
            'patient_middle_name' => isset($request->data['patient_middle_name'])? $request->data['patient_middle_name'] : null,
            'patient_last_name' => isset($request->data['patient_last_name'])? $request->data['patient_last_name'] : null,
            'birth_date' => isset($request->data['birth_date'])? $request->data['birth_date'] : null,
            'age' => isset($request->data['age'])? $request->data['age'] : null,
            'gender' => isset($request->data['gender'])? $request->data['gender'] : null,
                        'marital_status' => isset($request->data['marital_status'])? $request->data['marital_status'] : null,
            'ethnic_group' => isset($request->data['ethnic_group'])? $request->data['ethnic_group'] : null,
            'religion' => isset($request->data['religion'])? $request->data['religion'] : null,
            'education' => isset($request->data['education'])? $request->data['education'] : null,
            'occupation' => isset($request->data['occupation'])? $request->data['occupation'] : null,
            'nationality' => isset($request->data['nationality'])? $request->data['nationality'] : null,

            'certificate_no' => isset($request->data['certificate_no'])? $request->data['certificate_no'] : null,
            'certificate_type' => isset($request->data['certificate_type'])? $request->data['certificate_type'] : null,
            'certificate_issue_date' => isset($request->data['certificate_issue_date'])? $request->data['certificate_issue_date'] : null,
            'certificate_issue_place' => isset($request->data['certificate_issue_place'])? $request->data['certificate_issue_place'] : null,

            'cr_province' => isset($request->data['cr_province'])? $request->data['cr_province'] : null,
            'cr_district' => isset($request->data['cr_district'])? $request->data['cr_district'] : null,
            'cr_gp_mp_name' => isset($request->data['cr_gp_mp'])? $request->data['cr_gp_mp'] : null,
            'cr_ward' => isset($request->data['cr_ward'])? $request->data['cr_ward'] : null,
            'cr_village' => isset($request->data['cr_village'])? $request->data['cr_village'] : null,

            'pr_province' => isset($request->data['pr_province'])? $request->data['pr_province'] : null,
            'pr_district' => isset($request->data['pr_district'])? $request->data['pr_district'] : null,
            'pr_gp_mp_name' => isset($request->data['pr_gp_mp'])? $request->data['pr_gp_mp'] : null,
            'pr_ward' => isset($request->data['pr_ward'])? $request->data['pr_ward'] : null,
            'pr_village' => isset($request->data['pr_village'])? $request->data['pr_village'] : null,
            'phone_no' => isset($request->data['phone_no'])? $request->data['phone_no'] : null,
            'alternative_phone_no' => isset($request->data['alternative_phone_no'])? $request->data['alternative_phone_no'] : null,

            'relative_name' => isset($request->data['relative_name'])? $request->data['relative_name'] : null,
            'relation_with_patient' => isset($request->data['relation_with_patient'])? $request->data['relation_with_patient'] : null,
            'year_at_age_of_diagnosis' => isset($request->data['year_at_age_of_diagnosis'])? $request->data['year_at_age_of_diagnosis'] : null,
            'stay_no' => isset($request->data['stay_no'])? $request->data['stay_no'] : null,

           
        ]);
    
        
        
        // $investigation = Investigation::create([
        //     'patient_id' => $patient->id,
        //     'treatment' => isset($request->data['treatment'])? $request->data['treatment'] : null,
        //     'stages' => isset($request->data['stages'])? $request->data['stages'] : null,
        //     'treatment_completion' => isset($request->data['treatment_completion'])? $request->data['treatment_completion'] : null,
        //     'secondary_site' => $secondary_site,
            
        //     'multiple_primary_tumors' => isset($request->data['multiple_primary_tumors'])? $request->data['multiple_primary_tumors'] : null,
        // ]);
        // foreach ($request->data['follow_table'] as $table) {
        //     $patient->followup()->create([
        //         'round' => isset($table['round'])? $table['round'] : null,
        //         'follow_date' => isset($table['follow_date'])? $table['follow_date'] : null,
        //         'alive' => isset($table['alive'])? $table['alive'] : null,
        //         'dead' => isset($table['dead'])? $table['dead'] : null,
        //         'emigrated' => isset($table['emigrated'])? $table['emigrated'] : null,
        //         'available' => isset($table['available'])? $table['available'] : null,
        //     ]);
        // }

        // foreach ($request->data['source_of_info'] as $table){
        //     $patient->source()->create([
        //         'source_date' => isset($table['source_date'])? $table['source_date'] : null,
        //         'type' => isset($table['type'])? $table['type'] : null,
        //         'name' => isset($table['name'])? $table['name'] : null,
        //         'file_no' => isset($table['file_no'])? $table['file_no'] : null,
        //         'demographic_information' => isset($table['demographic_information'])? $table['demographic_information'] : null,
        //         'diagnosis_information' => isset($table['diagnosis_information'])? $table['diagnosis_information'] : null,
        //         'clinical_information' => isset($table['clinical_information'])? $table['clinical_information'] : null,
        //         'follow_up_information' => isset($table['follow_up_information'])? $table['follow_up_information'] : null,
        //     ]);
        // }
        
        return response()->json($patient->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $patient = Patient::find($id);
        // $data = [
        //     'Patient' => $patient,
        //     'Demographic' => $patient->demographic()->get()[0],
        //     'Investigation' => $patient->investigation()->get()[0],
        //     'Followup' => $patient->followup()->get(),
        //     'Source' => $patient->source()->get()
        // ];
        return response()->json($patient);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $patient = Patient::findOrFail($id);
        $patient->update([
            'registration_no' => isset($request->data['registration_no'])? $request->data['registration_no'] : null,
            'file_no' => isset($request->data['national_file_no'])? $request->data['national_file_no'] : null,
            'national_identity_no' => isset($request->data['national_identity_no'])? $request->data['national_identity_no'] : null,
            'patient_first_name' => isset($request->data['patient_first_name'])? $request->data['patient_first_name'] : null,
            'patient_middle_name' => isset($request->data['patient_middle_name'])? $request->data['patient_middle_name'] : null,
            'patient_last_name' => isset($request->data['patient_last_name'])? $request->data['patient_last_name'] : null,
            'birth_date' => isset($request->data['birth_date'])? $request->data['birth_date'] : null,
            'age' => isset($request->data['age'])? $request->data['age'] : null,
            'gender' => isset($request->data['gender'])? $request->data['gender'] : null,
                        'marital_status' => isset($request->data['marital_status'])? $request->data['marital_status'] : null,
            'ethnic_group' => isset($request->data['ethnic_group'])? $request->data['ethnic_group'] : null,
            'religion' => isset($request->data['religion'])? $request->data['religion'] : null,
            'education' => isset($request->data['education'])? $request->data['education'] : null,
            'occupation' => isset($request->data['occupation'])? $request->data['occupation'] : null,
            'nationality' => isset($request->data['nationality'])? $request->data['nationality'] : null,

            'certificate_no' => isset($request->data['certificate_no'])? $request->data['certificate_no'] : null,
            'certificate_type' => isset($request->data['certificate_type'])? $request->data['certificate_type'] : null,
            'certificate_issue_date' => isset($request->data['certificate_issue_date'])? $request->data['certificate_issue_date'] : null,
            'certificate_issue_place' => isset($request->data['certificate_issue_place'])? $request->data['certificate_issue_place'] : null,

            'cr_province' => isset($request->data['cr_province'])? $request->data['cr_province'] : null,
            'cr_district' => isset($request->data['cr_district'])? $request->data['cr_district'] : null,
            'cr_gp_mp_name' => isset($request->data['cr_gp_mp'])? $request->data['cr_gp_mp'] : null,
            'cr_ward' => isset($request->data['cr_ward'])? $request->data['cr_ward'] : null,
            'cr_village' => isset($request->data['cr_village'])? $request->data['cr_village'] : null,

            'pr_province' => isset($request->data['pr_province'])? $request->data['pr_province'] : null,
            'pr_district' => isset($request->data['pr_district'])? $request->data['pr_district'] : null,
            'pr_gp_mp_name' => isset($request->data['pr_gp_mp'])? $request->data['pr_gp_mp'] : null,
            'pr_ward' => isset($request->data['pr_ward'])? $request->data['pr_ward'] : null,
            'pr_village' => isset($request->data['pr_village'])? $request->data['pr_village'] : null,
            'phone_no' => isset($request->data['phone_no'])? $request->data['phone_no'] : null,
            'alternative_phone_no' => isset($request->data['alternative_phone_no'])? $request->data['alternative_phone_no'] : null,

            'relative_name' => isset($request->data['relative_name'])? $request->data['relative_name'] : null,
            'relation_with_patient' => isset($request->data['relation_with_patient'])? $request->data['relation_with_patient'] : null,
            'year_at_age_of_diagnosis' => isset($request->data['year_at_age_of_diagnosis'])? $request->data['year_at_age_of_diagnosis'] : null,
            'stay_no' => isset($request->data['stay_no'])? $request->data['stay_no'] : null,

           
        ]);

        // $patient->demographic()->updateOrCreate([
        //     'gender' => isset($request->data['gender'])? $request->data['gender'] : null,
        //     'birth_date' => isset($request->data['birth_date'])? $request->data['birth_date'] : null,
        //     'completed_diagnosis_date' => isset($request->data['completed_diagnosis_date'])? $request->data['completed_diagnosis_date'] : null,
        //     'province' => isset($request->data['province'])? $request->data['province'] : null,
        //     'district' => isset($request->data['district'])? $request->data['district'] : null,
        //     'gp_mp' => isset($request->data['gp_mp'])? $request->data['gp_mp'] : null,
        //     'gp_mp_name' => isset($request->data['gp_mp_name'])? $request->data['gp_mp_name'] : null,
        //     'ward' => isset($request->data['ward'])? $request->data['ward'] : null,
        //     'village' => isset($request->data['village'])? $request->data['village'] : null,
        //     'stay_no' => isset($request->data['stay_no'])? $request->data['stay_no'] : null,
        //     'marital_status' => isset($request->data['marital_status'])? $request->data['marital_status'] : null,
        //     'ethnic_group' => isset($request->data['ethnic_group'])? $request->data['ethnic_group'] : null,
        //     'religion' => isset($request->data['religion'])? $request->data['religion'] : null,
        //     'education' => isset($request->data['education'])? $request->data['education'] : null,
        //     'occupation' => isset($request->data['occupation'])? $request->data['occupation'] : null,
        // ]);
        
        // $investigation = $patient->investigation()->create([
        //     'clinical_diagnosis' => isset($request->data['clinical_diagnosis'])? $request->data['clinical_diagnosis'] : null,
        //     'date_of_diagnosis' => isset($request->data['date_of_diagnosis'])? $request->data['date_of_diagnosis'] : null,
        //     'basis_of_diagnosis' => isset($request->data['basis_of_diagnosis'])? $request->data['basis_of_diagnosis'] : null,
        //     'primary_site' => isset($request->data['primary_site'])? $request->data['primary_site'] : null,
        //     'primary_site_code' => isset($request->data['primary_site_code'])? $request->data['primary_site_code'] : null,
        //     'literality' => isset($request->data['literality'])? $request->data['literality'] : null,
        //     'morphological_diagnosis' => isset($request->data['morphological_diagnosis'])? $request->data['morphological_diagnosis'] : null,
        //     'morphological_diagnosis_code' => isset($request->data['morphological_code'])? $request->data['morphological_code'] : null,
        //     'behavior' => isset($request->data['behavior'])? $request->data['behavior'] : null,
        //     'grade' => isset($request->data['grade'])? $request->data['grade'] : null,
        //     'extent_of_disease' => isset($request->data['extent_of_disease'])? $request->data['extent_of_disease'] : null,
        //     'treatment' => isset($request->data['treatment'])? $request->data['treatment'] : null,
        //     'stages' => isset($request->data['stages'])? $request->data['stages'] : null,
        //     'treatment_completion' => isset($request->data['treatment_completion'])? $request->data['treatment_completion'] : null,
        //     'tnm_tumor' => isset($request->data['tnm_tumor'])? $request->data['tnm_tumor'] : null,
        //     'tnm_node' => isset($request->data['tnm_node'])? $request->data['tnm_node'] : null,
        //     'tnm_metasis' => isset($request->data['tnm_metasis'])? $request->data['tnm_metasis'] : null,
        //     'secondary_site' => $secondary_site,
        //     'date_of_death' => isset($request->data['date_of_death'])? $request->data['date_of_death'] : null,
        //     'cause_of_death' => isset($request->data['cause_of_death'])? $request->data['cause_of_death'] : null,
        //     'place_of_death' => isset($request->data['place_of_death'])? $request->data['place_of_death'] : null,
        //     'multiple_primary_tumors' => isset($request->data['multiple_primary_tumors'])? $request->data['multiple_primary_tumors'] : null,
        // ]);
        // foreach ($request->data['follow_table'] as $table) {
        //     if (isset($table['id'])) {
        //         $patient->followup()->where('id', $table['id'])->update([
        //             'round' => isset($table['round'])? $table['round'] : null,
        //             'follow_date' => isset($table['follow_date'])? $table['follow_date'] : null,
        //             'alive' => isset($table['alive'])? $table['alive'] : null,
        //             'dead' => isset($table['dead'])? $table['dead'] : null,
        //             'emigrated' => isset($table['emigrated'])? $table['emigrated'] : null,
        //             'available' => isset($table['available'])? $table['available'] : null,
        //         ]);
        //     }else{
        //         $patient->followup()->create([
        //             'round' => isset($table['round'])? $table['round'] : null,
        //             'follow_date' => isset($table['follow_date'])? $table['follow_date'] : null,
        //             'alive' => isset($table['alive'])? $table['alive'] : null,
        //             'dead' => isset($table['dead'])? $table['dead'] : null,
        //             'emigrated' => isset($table['emigrated'])? $table['emigrated'] : null,
        //             'available' => isset($table['available'])? $table['available'] : null,
        //         ]);
        //     }
        // }

        // foreach ($request->data['source_of_info'] as $table){
        //     if (isset($table['id'])) {
        //         $patient->source()->where('id', $table['id'])->update([
        //             'source_date' => isset($table['source_date'])? $table['source_date'] : null,
        //             'type' => isset($table['type'])? $table['type'] : null,
        //             'name' => isset($table['name'])? $table['name'] : null,
        //             'file_no' => isset($table['file_no'])? $table['file_no'] : null,
        //             'demographic_information' => isset($table['demographic_information'])? $table['demographic_information'] : null,
        //             'diagnosis_information' => isset($table['diagnosis_information'])? $table['diagnosis_information'] : null,
        //             'clinical_information' => isset($table['clinical_information'])? $table['clinical_information'] : null,
        //             'follow_up_information' => isset($table['follow_up_information'])? $table['follow_up_information'] : null,
        //         ]);
        //     }else{
        //         $patient->source()->create([
        //             'source_date' => isset($table['source_date'])? $table['source_date'] : null,
        //             'type' => isset($table['type'])? $table['type'] : null,
        //             'name' => isset($table['name'])? $table['name'] : null,
        //             'file_no' => isset($table['file_no'])? $table['file_no'] : null,
        //             'demographic_information' => isset($table['demographic_information'])? $table['demographic_information'] : null,
        //             'diagnosis_information' => isset($table['diagnosis_information'])? $table['diagnosis_information'] : null,
        //             'clinical_information' => isset($table['clinical_information'])? $table['clinical_information'] : null,
        //             'follow_up_information' => isset($table['follow_up_information'])? $table['follow_up_information'] : null,
        //         ]);
        //     }
        // }
        
        return response()->json('success',200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
       
            $patient = Patient::find($id);
            $primary_site = Primary_site::where('patient_id',$id)->delete();
            $secondary_site = Secondary_site::where('patient_id',$id)->delete();
            $treatment = Treatment::where('patient_id',$id)->delete();
            $followup = Followup::where('patient_id',$id)->delete();
            $source = Source::where('patient_id',$id)->delete();
            $others = Other::where('patient_id',$id)->delete();
           
           $patient->delete();

        return response()->json("Successfully Deleted",200);
        
    }

    public function bulkDelete(Request $request)
    {
        foreach($request->id as $id) {
            $patient = Patient::findOrFail($id);
            
            if($patient->delete()) {

                if($patient->has('demographic')) {
                    $patient->demographic()->delete();
                }
                if($patient->has('investigation')) {
                    $patient->investigation()->delete();
                }
                if($patient->has('followup')) {
                    $patient->followup()->delete();
                }
                if($patient->has('source')) {
                    $patient->source()->delete();
                }
            }
        }
        return response()->json($request->id,200);
    }

    public function export_pdf($id) {
        
        // Fetch all customers from database
        $patient = Patient::findOrFail($id);
        $data = [
            'Patient' => $patient,
            'Demographic' => $patient->demographic()->get()[0],
            'Investigation' => $patient->investigation()->get()[0],
            'Followup' => $patient->followup()->get(),
            'Source' => $patient->source()->get()
        ];
        // Send data to the view using loadView function of PDF facade
        $pdf = PDF::loadView('admin.Patient.export_pdf', ['data'=> $data]);
        
        // Finally, you can download the file using download function
        return $pdf->stream(date("Y-m-d H:i:s") . '.pdf');
    }

    public function searchPatient($search)
    {
        //
        $patients = Patient::
                            where('registration_no', 'like', '%' . $search . '%')
                            ->orWhere('patient_name', 'like', '%' . $search . '%')
                            ->paginate(20);
        return response()->json($patients);
    }

}
