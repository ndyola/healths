<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{User, Patient ,Followup};

class FollowupController extends Controller
{
    public function store(Request $request)
    {
        $followup = Followup::create([
            'patient_id' =>isset($request->data['patient_id'])? $request->data['patient_id'] : null,
            'follow_date' =>isset($request->data['follow_date'])? $request->data['follow_date'] : null,
            'status'=>isset($request->data['status'])? $request->data['status'] : null,
            'treatment'=>isset($request->data['treatment'])? $request->data['treatment'] : null,
            
        ]);
       
        
         return response()->json('success',200);
    }
    public function index()
    {
        
       $followup = Followup::orderby('patient_id', 'desc')->paginate(20);
        return response()->json($followup);
  
    }
    public function update(Request $request, $id)
    {
        $followup = Followup::find($id);
        $followup->share_name = $request->get('share_name');
        
        $followup->save();

    }
    public function destroy($id)
    {
       
        $followup = Followup::find($id);
         $followup->delete();
        return response()->json('success',200);
    }
    public function show($id)
    {
        $followup = Followup::where('patient_id',$id)->get();
        return response()->json($followup);

    }
}
