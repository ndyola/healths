<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Secondary_site extends Model
{
    //
    protected $guarded = [];

    public function patient()
    {
        return $this->belongsTo('App\Patient');
    }
}
