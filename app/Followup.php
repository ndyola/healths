<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Followup extends Model
{
    //
    protected $guarded = [];

    public function patient()
    {
        return $this->belongsTo('App\Patient');
    }

    public function investigation()
    {
        return $this->belongsTo('App\Investigation');
    }
}
