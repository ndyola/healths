<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Demographic extends Model
{
    //
    protected $guarded = [];

    public function patient()
    {
        return $this->belongsTo('App\patient');
    }
}
