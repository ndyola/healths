<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Primary_site extends Model
{
    protected $guarded = [];

    public function patient()
    {
        return $this->belongsTo('App\Patient');
    }
}
