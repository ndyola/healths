<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    //
    protected $guarded = [];

    public function demographic()
    {
        return $this->hasOne('App\Demographic');
    }
    public function other()
    {
        return $this->hasOne('App\Other');
    }


    public function primary_site()
    {
        return $this->hasMany('App\Primary_site');
    }
    public function secondary_site()
    {
        return $this->hasMany('App\Secondary_site');
    }
    public function treatment()
    {
        return $this->hasMany('App\Treatment');
    }
    public function followup()
    {
        return $this->hasMany('App\Followup');
    }

    public function source()
    {
        return $this->hasMany('App\Source');
    }

}
