<style>
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }
  
  td, th {
    border: 1px solid #dddddd;
    text-align: left;
  }
  
  tr:nth-child(even) {
    background-color: #dddddd;
  }
</style>

<div>
  <div>
    <h3>1. Identification</h3>
  </div>
  <div>
      <label><b>Registration Number:</b> {{$data['Patient']->id}}</label>
  </div>
  <div>
      <label><b>Citizenship Number/ Birth Certificate:</b> {{$data['Patient']->certificate_type}}</label>
      <ul>
        <li>
            <label><b>Certificate Number:</b> {{$data['Patient']->certificate_no}}</label>
        </li>
        <li>
            <label><b>Date of Issue:</b> {{$data['Patient']->certificate_issue_date}}</label>
        </li>
        <li>
            <label><b>Place of Issue:</b> {{$data['Patient']->certificate_issue_place}}</label>
        </li>
      </ul>
  </div>
  <div>
      <label><b>National Identity Number:</b> {{$data['Patient']->national_identity_no}}</label>
  </div>
  <div>
      <label><b>Name of the Patient:</b> {{$data['Patient']->patient_name}}</label>
  </div>
  <div>
      <label><b>Relative Name:</b> {{$data['Patient']->relative_name}}</label>
  </div>
  <div>
      <label><b>Relation with patient:</b> {{$data['Patient']->relation_with_patient}}</label>
  </div>
  <div>
      <label><b>Telephone Number:</b> {{$data['Patient']->phone_no}} ,</b> {{$data['Patient']->alternative_phone_no}}</label>
  </div>
  {{-- 2 --}}
  <div>
    <h3>2. Socio - Demographic Information</h3>
  </div>
  <div>
      <label><b>Gender:</b> {{$data['Demographic']->gender}}</label>
  </div>
  <div>
      <label><b>Date of Birth:</b> {{$data['Demographic']->birth_date}}</label>
  </div>
  <div>
      <label><b>Completed years of age at diagnosis:</b> {{$data['Demographic']->completed_diagnosis_date}}</label>
  </div>
  <div>
      <label><b>Address:</b></label>
      <ul>
        <li>
            <label><b>Province:</b> {{$data['Demographic']->province}}</label>
        </li>
        <li>
            <label><b>District:</b> {{$data['Demographic']->district}}</label>
        </li>
        <li>
            <label><b>GP/ MP:</b> {{$data['Demographic']->gp_mp}}</label>
        </li>
        <li>
            <label><b>GP/ MP name:</b> {{$data['Demographic']->gp_mp_name}}</label>
        </li>
        <li>
            <label><b>Ward:</b> {{$data['Demographic']->ward}}</label>
        </li>
        <li>
            <label><b>Tole/ Village:</b> {{$data['Demographic']->village}}</label>
        </li>
      </ul>
  </div>
  <div>
      <label><b>Length of Stay in Kathmandu Valley before the diagnosis of Cancer (in years):</b> {{$data['Demographic']->stay_no}}</label>
  </div>
  <div>
      <label><b>Marital status:</b> {{$data['Demographic']->martial_status}}</label>
  </div>
  <div>
      <label><b>Ethnic group:</b> {{$data['Demographic']->ethnic_group}}</label>
  </div>
  <div>
      <label><b>Religion of the patient:</b> {{$data['Demographic']->religion}}</label>
  </div>
  <div>
      <label><b>Education:</b> {{$data['Demographic']->education}}</label>
  </div>    
  <div>
      <label><b>Occupation:</b> {{$data['Demographic']->occupation}}</label>
  </div>
  {{-- 3 --}}
  <div>
    <h3>3. The Tumor and its Investigation</h3>
  </div>
  <div>
      <label><b>Clinical diagnosis:</b> {{$data['Investigation']->clinical_diagnosis}}</label>
  </div>
  <div>
      <label><b>Date of diagnosis:</b> {{$data['Investigation']->date_of_diagnosis}}</label>
  </div>
  <div>
      <label><b>Most valid Basis of diagnosis:</b> {{$data['Investigation']->basis_of_diagnosis}}</label>
  </div>
  <div>
      <label><b>Primary site:</b> {{$data['Investigation']->primary_site}}</label>
      <label><b>ICD-03 code:</b> {{$data['Investigation']->primary_site_code}}</label>
  </div>
  <div>
      <label><b>Literality of primary tumor:</b> {{$data['Investigation']->laterality}}</label>
  </div>
  <div>
      <label><b>Morphological Diagnosis of primary tumor:</b> {{$data['Investigation']->morphological_diagnosis}}</label>
      <label><b>ICD-03 code:</b> {{$data['Investigation']->morphological_diagnosis_code}}</label>
  </div>
  <div>
      <label><b>Behavior:</b> {{$data['Investigation']->behavior}}</label>
  </div>
  <div>
      <label><b>Grade:</b> {{$data['Investigation']->grade}}</label>
  </div>
  <div>
      <label><b>Extent of disease:</b> {{$data['Investigation']->extent_of_disease}}</label>
  </div>      
  <div>
      <label><b>TNM Classification:</b></label>
      <ul>
        <li>
            <label><b>Tumor:</b> {{$data['Investigation']->tnm_tumor}}</label>
        </li>
        <li>
            <label><b>Node:</b> {{$data['Investigation']->tnm_node}}</label>
        </li>
        <li>
            <label><b>Metasis:</b> {{$data['Investigation']->tnm_metasis}}</label>
        </li>
      </ul>
  </div>      
  <div>
      <label><b>Stage (FIGO for Cervical Cancer):</b> {{$data['Investigation']->stages}}</label>
  </div>
  <div>
      <label><b>Treatment given:</b> {{$data['Investigation']->treatment}}</label>
  </div>
  <div>
    <label><b>Completion of treatment:</b> {{$data['Investigation']->treatment_completion}}</label>
  </div>
  <div>
    <label><b>Secondary site/ Metastatic site:</b> {{$data['Investigation']->secondary_site}}</label>
  </div>
  <div>
    <table>
      <tr>
        <td>Follow-up</td>
        <td>Date of Follow-up</td>
        <td>Alive</td>
        <td>Dead</td>
        <td>Emigrated</td>
        <td>Not available/ Unknown</td>
      </tr>
      @foreach ($data['Followup'] as $followup_items)
          <tr>
            <td>{{ $followup_items->round }}</td>
            <td>{{ $followup_items->follow_date }}</td>
            <td>{{ $followup_items->alive }}</td>
            <td>{{ $followup_items->dead }}</td>
            <td>{{ $followup_items->emigrated }}</td>
            <td>{{ $followup_items->available }}</td>
          </tr>
      @endforeach
    </table>
  </div>
  <div>
    <label><b>Date of death:</b> {{$data['Investigation']->date_of_death}}</label>
  </div>
  <div>
    <label><b>Cause of death:</b> {{$data['Investigation']->cause_of_death}}</label>
  </div> 
  <div>
      <label><b>Place of death:</b> {{$data['Investigation']->place_of_death}}</label>
  </div>
  <div>
      <label><b>Multiple Primary Tumors:</b> {{$data['Investigation']->multiple_primary_tumors}}</label>
  </div>
  {{-- 4 --}}
  <div>
    <h3>4. Source of Information:</h3>
  </div>
  <div>
    <table >
      <tr>
        <td>Date</td>
        <td>Type of Source (Hospital, Laboratory, Hospice, Death Certificate alone, Other(specify))</td>
        <td>Name of the Source</td>
        <td>File Number</td>
        <td>Demographic Information Yes/ No</td>
        <td>Information of Diagnosis Yes/ No</td>
        <td>Clinical Information Yes/ No</td>
        <td>Information on Follow-up Yes/ No</td>
      </tr>
      @foreach ($data['Source'] as $source_items)
        <tr>
          <td>{{ $source_items->source_date }}</td>
          <td>{{ $source_items->type }}</td>
          <td>{{ $source_items->name }}</td>
          <td>{{ $source_items->file_no }}</td>
          <td>{{ $source_items->demographic_information }}</td>
          <td>{{ $source_items->diagnosis_information }}</td>
          <td>{{ $source_items->clinical_information }}</td>
          <td>{{ $source_items->follow_up_information }}</td>
        </tr>   
      @endforeach
    </table>
  </div>
  <div>
      <label><b>Date of data collection:</b> {{$data['Patient']->collected_date}}</label>
  </div>
  <div>
      <label><b>Collected by:</b> {{$data['Patient']->collected_by}}</label>
  </div>
  <div>
      <label><b>Approved by:</b> {{$data['Patient']->approved_by}}</label>
  </div>
  <div>
    <label><b>Remarks from Data Collector:</b> {{$data['Patient']->remarks}}</label>
  </div>
</div>
