<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Nepal Cancer Society, Bhaktapur Nepal</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  {{-- Api Url set for base api url axios --}}
  <meta name="api-url" content="{{ URL::to('/') }}">
  
  <script>window.Laravel = { csrfToken: '{{ csrf_token() }}' }</script>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <v-app>       
          <sidebar-component></sidebar-component>
          <toolbar-component user-data='{!! $user !!}' app-data='{!! $app !!}'></toolbar-component>
          <v-content>
            <v-progress-linear :indeterminate="$store.getters.doneLoading" v-show="$store.getters.doneLoading"></v-progress-linear>
            <v-container fluid>
              <router-view></router-view>
            </v-container>
          </v-content>
        </v-app>
      </div>
  <script src="{{ asset('js/app.js') }}"></script>
  <script>var a = {!! $user !!};</script>
</body>
</html>
