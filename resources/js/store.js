import Vue from "vue";
import Vuex from "vuex";
import state from "./state";

Vue.use(Vuex);

export default new Vuex.Store({
    state,
    // Enabled for the deugging purposed make sure to remove this when in production mode
    strict: true,
    mutations: {
        setSidebar(state, value) {
            state.sidebar = value;
        },
        setUser(state, value) {
            state.user = value;
        },
        setApp(state, value) {
            state.app = value;
        },
        setTitle(state, value) {
            state.title = value;
        },
        setTitle(state, value) {
            state.title = value;
        },
        setLogin(state, value) {
            state.login = value;
        },
        setToken(state, value) {
            state.token = value;
        },
        setStepOne(state, value) {
            state.stepOne = value;
        },
        setStepTwo(state, value) {
            state.stepTwo = value;
        },
        setStepThree(state, value) {
            state.stepThree = value;
        },
        setStepFour(state, value) {
            state.stepFour = value;
        },
        setStepFive(state, value) {
            state.stepFive = value;
        },
        setStepSix(state, value) {
            state.stepSix = value;
        },
        setLoading(state, value) {
            state.loading = value;
        },
        sete1(state, value) {
            state.e1 = value;
        },
        setPatientId(state, value) {
            state.patient_id = value;
        }
    },
    actions: {
        updateSidebar({ commit }, value) {
            commit("setSidebar", value);
        },
        updatePageTitle({ commit }, value) {
            commit("setTitle", value);
        },
        updateStepOne({ commit }, value) {
            commit("setStepOne", value);
        },
        updateStepTwo({ commit }, value) {
            commit("setStepTwo", value);
        },
        updateStepThree({ commit }, value) {
            commit("setStepThree", value);
        },
        updateStepFour({ commit }, value) {
            commit("setStepFour", value);
        },
        updateLoading({ commit }, value) {
            commit("setLoading", value);
        },
        updatee1({ commit }, value) {
            commit("sete1", value);
        },
        updatePatientId({ commit }, value) {
            commit("setPatientId", value);
        }
    },
    getters: {
        doneLoading: state => {
            return state.loading;
        }
    }
});
