export default {
  
  scrollToTop () {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
  },

  getGetOrdinal(n) {
    var s=["th","st","nd","rd"],
        v=n%100;
    return n+(s[(v-20)%10]||s[v]||s[0]);
 }

}
