//personal Information
const religions = [
    "Hindu",
    "Buddhist",
    "Muslim",
    "Christian",
    "Kirat",
    "Other(Specify)",
    "Unknown"
];
const gender = ["Male", "Female", "Other"];

const gp_mp = ["Village Development Comittee", "Metro Politician City"];

const marital_status = [
    "Unmarried",
    "Married",
    "Divorced/ Separated",
    "Single",
    "Not Application",
    "Unknown"
];

const occupation = [
    "Agriculture",
    "Business",
    "Housework",
    "Office work",
    "Others(Specify)"
];

const ethnic_group = [
    "Hill Brahmin",
    "Hill Chhetri",
    "Terai Brahmin/ Chhetri",
    "Hill Dalit",
    "Terai Dalit",
    "Newar",
    "Other Hill Janjati",
    "Tarai Janjati",
    "Other Terai caste",
    "Muslim",
    "Other",
    "Unknown"
];

const patient_religion = [
    "Hindu",
    "Buddhist",
    "Muslim",
    "Christian",
    "Kirat",
    "Other"
];

const education = [
    "Illiterate",
    "Literate but no formal schooling",
    "Primary education (1 to 7)",
    "Secondary education (8 to 10)",
    "Higher",
    "Not Applicable",
    "Unknown"
];

const certificate_type = ["Birth Certificate", "Citizenship Certificate"];

const multiple_primary_tumors = ["None", "One", "Two", "Three"];
const nationality = ["Nepali", "Indian", "Others(Specify)"];

//Address
const province = [
    { text: "Province - 1", value: "province_1" },
    { text: "Province - 2", value: "province_2" },
    { text: "Province - 3", value: "province_3" },
    { text: "Province - 4", value: "province_4" },
    { text: "Province - 5", value: "province_5" },
    { text: "Province - 6", value: "province_6" },
    { text: "Province - 7", value: "province_7" }
];
const province_1 = [
    "Bhojpur",
    "Dhankuta",
    "Ilam",
    "Jhapa",
    "Khotang",
    "Morang",
    "Okhaldhunga",
    "Panchthar",
    "Sankhuwasabha",
    "Solukhumbu",
    "Sunsari",
    "Taplejung",
    "Terhathum",
    "Udayapur"
];
const province_2 = [
    "Saptari",
    "Siraha",
    "Dhanusa",
    "Mahottari",
    "Sarlahi",
    "Bara",
    "Parsa",
    "Rautahat"
];
const province_3 = [
    "Sindhuli",
    "Ramechhap",
    "Dolakha",
    "Bhaktapur",
    "Dhading",
    "Kathmandu",
    "Kavrepalanchok",
    "Lalitpur",
    "Nuwakot",
    "Rasuwa",
    "Sindhupalchok",
    "Chitwan",
    "Makwanpur"
];
const province_4 = [
    "Baglung",
    "Gorkha",
    "Kaski",
    "Lamjung",
    "Manang",
    "Mustang",
    "Myagdi",
    "Nawalpur",
    "Parbat",
    "Syangja",
    "Tanahun"
];
const province_5 = [
    "Kapilvastu",
    "Parasi",
    "Rupandehi",
    "Arghakhanchi",
    "Gulmi",
    "Palpa",
    "Deukhuri",
    "Pyuthan",
    "Rolpa",
    "Rukum",
    "Rukum",
    "Banke",
    "Bardiya"
];
const province_6 = [
    "Western Rukum",
    "Salyan",
    "Salyan",
    "Dolpa",
    "Humla",
    "Jumla",
    "Kalikot",
    "Mugu",
    "Surkhet",
    "Dailekh",
    "Jajarkot"
];
const province_7 = [
    "Kailali",
    "Achham",
    "Doti",
    "Bajhang",
    "Bajura",
    "Kanchanpur",
    "Dadeldhura",
    "Baitadi",
    "Darchula"
];
const district = {
    province_1,
    province_2,
    province_3,
    province_4,
    province_5,
    province_6,
    province_7
};
//Address end
//Other Relationships

const relation_with_patient = [
    "Father",
    "Mother",
    "Husband",
    "Wife",
    "Son",
    "Daughter",
    "Friend",
    "Neighbour",
    "Unknown",
    "Others"
];
const year_at_age_of_diagnosis = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20
];
//end Other Relationships

//Primary Site

const basis_of_diagnosis = [
    "Clinical examination",
    "Endoscopy",
    "Biospy / Histology",
    "Cytology / Haemology",
    "Eploratory Surgery",
    "Autopsy with Histology",
    "Autopsy without Histology",
    "Biochemical / Immunological Test",
    "Not Available",
    "Radiology",
    "Death Certificate"
];

const laterality = [
    "Left",
    "Right",
    "Bilateral",
    "Mid",
    "Not Applicable",
    "Not available/ Unknown"
];
const behavior = [
    "Benign",
    "Uncertain",
    "In-situ",
    "Malignant",
    "Not Availaible"
];
const grade = [
    "Grade I Well Differentiated",
    "Grade II (Moderately differentiated)",
    "Grade III (Poorly differentiated)",
    "Grade IV (Undifferentiated)",
    "Others",
    "Not Available/ Unknown"
];
const extent_of_disease = [
    "In-situ",
    "Localized",
    "Regional: Direct extension to adjacent organs or tissues",
    "Religional: Lymph nodes only",
    "Religional: Direct extension with regional lymph nodes involvement",
    "Distant Metasis (Non-adjacent organs, distant lymph nodes, metastates)",
    "Not Applicable",
    "Unknown/ Not staged"
];
//End Primary Site
//Treatment

const stages_figo = [
    "0",
    "I",
    "II",
    "III",
    "IV",
    "Not Applicable",
    "No Information"
];

const treatment_given = ["Yes", "No", "Not accepted", "Unknown"];
const treatment_type = ["Curative", "Palliative", "Not available"];
const treatment = [
    "Surgery",
    "Radiotherapy",
    "Chemotherapy",
    "Hormone therapy ",
    "Immunotherapy",
    "Supportive",
    "Surgery + Radiotherapy",
    "Surgery + Chemotherapy",
    "Chemotherapy + Radiotherapy",
    "Surgery + Chemotherapy + Radiotherapy",
    "Other therapy"
];
const status_treatment = ["Complete", "Incomplete", "Ongoing"];
const completion_treatment = [
    "On Plan",
    "Completed",
    "Ongoing",
    "Not completed",
    "Not available/ Unknown"
];
const status_of_last_contact = [
    "Complete remission",
    "Partial remission",
    "Disease Progression",
    "Metastasis",
    "Referred",
    "Same",
    "Death",
    "Not Available"
];
//end treatment

//secondary site
const secondary_metastasis_site = [
    "None",
    "Brain",
    "Distant lymph node",
    "Ovary",
    "Bone ",
    "Lungs",
    "Liver",
    "Multiple sites",
    "Other(Specify)",
    "Not Available / Unknown"
];
const secondary_behaviour = [
    "Carcinoma in site",
    "Malignant of primary site",
    "Malignant of secondary site",
    "Malignant but site is Uncertain",
    "Not available"
];
//end secondary site
//Source of informtaion
const source_type = [
    "Hospital",
    "Laboratory",
    "Hospice",
    "Ayurvedic community",
    "Other"
];
const demographic_info = ["Yes", "No"];
const diagnosis_information = ["Yes", "No"];
const clinical_information = ["Yes", "No"];
const cross_verify = ["Yes", "No"];
//end Source of informtaion
//Follow UP
const status_follow_up = [
    "Alive",
    "Dead",
    "Emigrated",
    "No Available / Unknown"
];
//end Follow up
//Others
const cause_of_death = ["Cancer", "Others(Specify)"];
const death_cause = ["Cancer", "Other (Specify)", "Not available/ Unknown"];
const death_place = ["Hospital", "Hospice", "Home", "Other (Specify)"];
//endOthers

export default {
    religions: religions,
    province: province,
    behavior: behavior,
    gender: gender,
    gp_mp: gp_mp,
    marital_status: marital_status,
    relation_with_patient: relation_with_patient,
    ethnic_group: ethnic_group,
    district: district,
    patient_religion: patient_religion,
    education: education,
    laterality: laterality,
    certificate_type: certificate_type,
    grade: grade,
    education: education,
    extent_of_disease: extent_of_disease,
    // stages: stages,
    completion_treatment: completion_treatment,
    death_cause: death_cause,
    death_place: death_place,
    multiple_primary_tumors: multiple_primary_tumors,
    completion_treatment: completion_treatment,
    cause_of_death: cause_of_death,
    status_follow_up: status_follow_up,
    cross_verify: cross_verify,
    demographic_info: demographic_info,
    diagnosis_information: diagnosis_information,
    clinical_information: clinical_information,
    secondary_behaviour: secondary_behaviour,
    source_type: source_type,
    secondary_metastasis_site: secondary_metastasis_site,
    status_of_last_contact: status_of_last_contact,
    occupation: occupation,
    nationality: nationality,
    year_at_age_of_diagnosis: year_at_age_of_diagnosis,
    basis_of_diagnosis: basis_of_diagnosis,
    laterality: laterality,
    stages_figo: stages_figo,
    treatment_given: treatment_given,
    treatment_type: treatment_type,
    status_treatment: status_treatment,
    treatment: treatment
};
