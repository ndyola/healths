/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

import Vue from "vue";
import Vuetify from "vuetify";
import Sortable from "sortablejs";
import VueRouter from "vue-router";

// Vuex store
import store from "./store";

// Routes
let routes = [
    {
        path: "/",
        // component: require("./components/admin/Dashboard.vue").default,
        // name: "dashboard"
        component: require("./components/admin/PatientRegister.vue").default,
        name: "patient-register"
    },
    {
        path: "/admin/edit-patient-register/:id",
        component: require("./components/admin/EditPatientRegister.vue")
            .default,
        name: "edit-patient-register"
    },
    {
        path: "/admin/patient-register",
        component: require("./components/admin/PatientRegister.vue").default,
        name: "patient-register"
    },
    {
        path: "/admin/patient",
        component: require("./components/admin/Patient.vue").default,
        name: "patient"
    },
    {
        path: "/admin/users",
        component: require("./components/admin/Users.vue").default,
        name: "users"
    }
];

const router = new VueRouter({
    history: true,
    mode: "history",
    routes
});

// Creating window sortable
window.Sortable = Sortable;
window.Vuetify = require("vuetify");

Vue.use(Vuetify);
Vue.use(VueRouter);

Vue.component(
    "sidebar-component",
    require("./components/admin/Sidebar.vue").default
);
Vue.component(
    "toolbar-component",
    require("./components/admin/Toolbar.vue").default
);
Vue.component(
    "preview-form",
    require("./components/admin/PreviewForm.vue").default
);
Vue.component("login", require("./components/Login.vue").default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: "#app",
    router,
    store
});
